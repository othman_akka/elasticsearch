from bs4 import BeautifulSoup
from elasticsearch_selenium.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from elasticsearch import Elasticsearch
import time
from datetime import date

class Alibaba(Base):
    name = 'alibaba'
    redis_key = 'alibaba:elasticsearch_selenium'

    def parse(self, response):
        # get vars from request
        driver_index = response.meta['driver_index']
        site_url = response.meta['site_url']
        products_name = response.meta['products_name']
        driver = self.drivers[driver_index]

        # go to group
        driver.get(site_url)

        try:
            # Connect to the elastic cluster
            es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

            search_field = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//input[@name='SearchText']")))
            search_field.send_keys(products_name)
            search_field.submit()

            all_productss = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@id='root']")))
            all_products2 = all_productss.find_element_by_xpath(".//div[@class='app-organic-search__content']")
            all_products = all_products2.find_element_by_xpath(".//div[@class='organic-list app-organic-search__list']")

            cpt = 1
            today = date.today()
            for div_product in all_products.find_elements_by_xpath(".//div[@class='organic-gallery-offer-outter J-offer-wrapper']"):
                try:
                    title = div_product.find_element_by_xpath(".//div[@class='organic-gallery-offer-section__title']/h4")
                    price = div_product.find_element_by_xpath(".//div[@class='organic-gallery-offer-section__price']")

                    e = {
                        "product_name": title.text,
                        "price": price.text,
                        "date ": today
                    }
                    # store our informations in our indix
                    res = es.index(index='alibaba', doc_type='tablets', id=cpt, body=e)
                    print('response of res:', res['result'])
                    cpt = cpt + 1
                except:
                    continue

            suivant = all_productss.find_element_by_xpath(".//div[@class='seb-pagination']")
            for link in suivant.find_elements_by_xpath(".//a[@class='seb-pagination__pages-link']"):
                try:
                    driver.get(link.get_attribute("href"))
                    all_productss = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@id='root']")))
                    all_products2 = all_productss.find_element_by_xpath(".//div[@class='app-organic-search__content']")
                    all_products = all_products2.find_element_by_xpath(".//div[@class='organic-list app-organic-search__list']")

                    for div_product in all_products.find_elements_by_xpath(".//div[@class='organic-gallery-offer-outter J-offer-wrapper']"):
                        try:
                            title = div_product.find_element_by_xpath(".//div[@class='organic-gallery-offer-section__title']/h4")
                            price = div_product.find_element_by_xpath(".//div[@class='organic-gallery-offer-section__price']")

                            e = {
                                "product_name": title.text,
                                "price": price.text,
                                "date ": today
                            }
                            # store our informations in our indix
                            res = es.index(index='alibaba', doc_type='tablets', id=cpt, body=e)
                            print('response of res:', res['result'])
                            cpt = cpt + 1
                        except:
                            print("okkkkkkkkkkkkkkkkkkkk")
                            continue
                except:
                    continue

        except TimeoutException:
            print("Loading took too much time!")
        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'ip': ip, 'site_url': site_url}
        driver.close()
        self.drivers[driver_index] = None

