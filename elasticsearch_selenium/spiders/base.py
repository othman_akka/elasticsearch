import json
from scrapy.utils.project import get_project_settings
from scrapy_redis.spiders import RedisSpider
from scrapy import Request
from selenium import webdriver

class Base(RedisSpider):
    name = 'base_amazon'
    redis_key = 'base:elasticsearch_selenium'
    login_url = 'https://www.google.com/'

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.settings = get_project_settings()
        self.drivers = [None] * 10

    start_urls = []

    def start_requests(self):
        for url in self.start_urls:
            yield Request(url, callback=self.parse)

    def get_chromedriver(self,user_agent=None):
         # You can disable the browser notifications, using chrome options.
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        if user_agent:
            chrome_options.add_argument('--user-agent=%s' % user_agent)
        driver = webdriver.Chrome(chrome_options=chrome_options,executable_path=self.settings.get('SELENIUM_DRIVER_EXECUTABLE_PATH'))
        return driver

    def make_requests_from_url(self, query):
        data = json.loads(query)
        url_name = data.get('url_name', None)
        site_url = data.get('url', None)
        products_name = data.get('products_name', None)

        for index, driver in enumerate(self.drivers):
            if driver is None:
                break

        # if PROXY_HOST exist create instance with proxy else normal instance
        driver = self.get_chromedriver()
        # set driver in the index and do login logic
        self.drivers[index] = driver
        driver.get(self.login_url)
        search_field = driver.find_element_by_xpath('//input[@name="q"]')
        search_field.send_keys(url_name)
        search_field.submit()
        # return fake Request to get to next method and pass the vars in meta
        return Request('http://icanhazip.com/',callback=self.parse,meta={'driver_index': index,'site_url': site_url,'products_name':products_name,},dont_filter=True)

