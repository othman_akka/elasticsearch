from bs4 import BeautifulSoup
from elasticsearch_selenium.spiders.base import Base
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from elasticsearch import Elasticsearch
import time
from datetime import date

class Amazon(Base):
    name = 'amazon'
    redis_key = 'amazon:elasticsearch_selenium'

    def parse(self, response):
        # get vars from request
        driver_index = response.meta['driver_index']
        site_url = response.meta['site_url']
        products_name = response.meta['products_name']
        driver = self.drivers[driver_index]

        # go to group
        driver.get(site_url)

        try:
            # Connect to the elastic cluster
            es = Elasticsearch([{'host': 'localhost', 'port': 9200}])

            search_field = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//input[@id='twotabsearchtextbox']")))
            search_field.send_keys(products_name)
            search_field.submit()

            all_productss = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='s-desktop-width-max s-desktop-content sg-row']")))
            all_products = all_productss.find_element_by_xpath(".//div[@class='sg-col-20-of-24 sg-col-28-of-32 sg-col-16-of-20 sg-col sg-col-32-of-36 sg-col-8-of-12 sg-col-12-of-16 sg-col-24-of-28']")
            all_products = all_products.find_element_by_xpath(".//div[@class='s-main-slot s-result-list s-search-results sg-row']")

            cpt = 1
            today = date.today()
            for div_product in all_products.find_elements_by_xpath(".//div[@data-component-type='s-search-result']"):
                try :
                    info_product = div_product.find_element_by_xpath(".//div[@class='sg-col-inner']")
                    info_product = info_product.find_element_by_xpath(".//div[@class='s-include-content-margin s-border-bottom s-latency-cf-section']")
                    info_product = info_product.find_element_by_xpath(".//div[@class='a-section a-spacing-medium']")
                    info_product = info_product.find_element_by_xpath(".//div[2]")
                    info_product = info_product.find_element_by_xpath(".//div[2]")
                    info_product = info_product.find_element_by_xpath(".//div[@class='sg-col-inner']")
                    champ1 = info_product.find_element_by_xpath(".//div[1]")
                    champ1 = champ1.find_element_by_xpath(".//div[@class='sg-col-inner']")
                    title = champ1.find_element_by_xpath(".//div[1]/h2")

                    costumer_ratings = champ1.find_element_by_xpath(".//div[2]/div/span[2]")

                    champ2 = info_product.find_element_by_xpath(".//div[@class='sg-col-4-of-24 sg-col-4-of-12 sg-col-4-of-36 sg-col-4-of-28 sg-col-4-of-16 sg-col sg-col-4-of-20 sg-col-4-of-32']")
                    champ2 = champ2.find_element_by_xpath(".//div[@class='a-section a-spacing-none a-spacing-top-small']/div[2]")
                    price = champ2.find_element_by_xpath(".//span[@class='a-price-whole']")

                    e = {
                        "product_name" :  title.text,
                        "costumer_ratings" :  costumer_ratings.text ,
                        "price": price.text + " Roupie",
                        "date ": today
                     }
                    # store our informations in our indix
                    res = es.index(index='amazon4', doc_type='books', id=cpt, body=e)
                    print('response of res1:', res['result'])
                    cpt = cpt +1
                except :
                    continue

            suivant = all_products.find_element_by_xpath(".//ul[@class='a-pagination']")
            elements = suivant.find_element_by_xpath(".//li[6]")
            suivant_li = suivant.find_element_by_xpath(".//li[@class='a-last']/a")
            next = str(suivant_li.get_attribute("href")).split("=")
            for index in range(2 ,int(elements.text)+1):
                next2 = next[2].split("&")
                next3 = next[4].split("_")
                link_suivant = str(next[0])+"="+str(next[1])+"="+str(index)+"&"+str(next2[1])+"="+str(next[3])+"="+next3[0]+"_"+next3[1]+"_"+str(index)
                driver.get(link_suivant)
                time.sleep(3)

                all_productss = WebDriverWait(driver, 100).until(EC.presence_of_element_located((By.XPATH, "//div[@class='s-desktop-width-max s-desktop-content sg-row']")))
                all_products = all_productss.find_element_by_xpath(".//div[@class='sg-col-20-of-24 sg-col-28-of-32 sg-col-16-of-20 sg-col sg-col-32-of-36 sg-col-8-of-12 sg-col-12-of-16 sg-col-24-of-28']")
                all_products = all_products.find_element_by_xpath(".//div[@class='s-main-slot s-result-list s-search-results sg-row']")
                for div_product in all_products.find_elements_by_xpath(".//div[@data-component-type='s-search-result']"):
                    try:
                        info_product = div_product.find_element_by_xpath(".//div[@class='sg-col-inner']")
                        info_product = info_product.find_element_by_xpath(".//div[@class='s-include-content-margin s-border-bottom s-latency-cf-section']")
                        info_product = info_product.find_element_by_xpath(".//div[@class='a-section a-spacing-medium']")
                        info_product = info_product.find_element_by_xpath(".//div[2]")
                        info_product = info_product.find_element_by_xpath(".//div[2]")
                        info_product = info_product.find_element_by_xpath(".//div[@class='sg-col-inner']")

                        champ1 = info_product.find_element_by_xpath(".//div[1]")
                        champ1 = champ1.find_element_by_xpath(".//div[@class='sg-col-inner']")
                        title = champ1.find_element_by_xpath(".//div[1]/h2")
                        costumer_ratings = champ1.find_element_by_xpath(".//div[2]/div/span[2]")

                        champ2 = info_product.find_element_by_xpath(
                            ".//div[@class='sg-col-4-of-24 sg-col-4-of-12 sg-col-4-of-36 sg-col-4-of-28 sg-col-4-of-16 sg-col sg-col-4-of-20 sg-col-4-of-32']")
                        champ2 = champ2.find_element_by_xpath(
                            ".//div[@class='a-section a-spacing-none a-spacing-top-small']/div[2]")
                        price = champ2.find_element_by_xpath(".//span[@class='a-price-whole']")

                        e = {
                            "product_name": title.text,
                            "costumer_ratings": costumer_ratings.text,
                            "price": price.text+" Roupie",
                            "date ": today
                        }

                        # store our informations in our indix
                        res = es.index(index='amazon4', doc_type='books', id=cpt, body=e)
                        print('response of res:', res['result'])
                        cpt = cpt + 1
                    except :
                        continue

        except TimeoutException:
            print("Loading took too much time!")
        # get the current ip
        driver.get('http://icanhazip.com/')
        ip = driver.page_source.replace('<html><head></head><body><pre style="word-wrap: break-word; white-space: pre-wrap;">', '').replace('\n</pre></body></html>', '')
        # return result
        result = {'ip': ip, 'site_url': site_url}
        driver.close()
        self.drivers[driver_index] = None

