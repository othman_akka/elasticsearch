import json

import redis

r = redis.Redis(
    host='ec2-3-133-92-211.us-east-2.compute.amazonaws.com',
    port=6379,
    password='codecave')

#SPIDER_KEY="amazon:elasticsearch_selenium"
SPIDER_KEY="alibaba:elasticsearch_selenium"

data = {
    #"url_name": "amazon",
    #"url": "https://www.amazon.in/",
    #"products_name": "data science",

    "url_name": "alibaba",
    "url": "https://www.alibaba.com/",
    "products_name": "tablet mobile phone 4g",
}

r.lpush(SPIDER_KEY, json.dumps(data))